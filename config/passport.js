const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const Usuario = require('../models/usuario');
var GoogleStrategy = require( 'passport-google-oauth2' ).Strategy;


passport.use(new LocalStrategy((email,password,done)=>{
    Usuario.findOne({email:email},(err,usuario)=>{
        if(err) return done(err);
        if(!usuario) return done(null,false,{message:'Email no existente o usuario incorrecto'});
        if(!usuario.validPassword(password)) return done(null,false,{message: 'Password incorrect'});

        return done(null,usuario);
    });
}));

passport.use(new GoogleStrategy({
    clientID:     process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET,
    callbackURL: process.env.HOST + "/auth/google/callback",
  },
  function(request, accessToken, refreshToken, profile, done) {
    User.findOrCreate({ googleId: profile.id }, function (err, user) {
      return done(err, user);
    });
  }
));

passport.serializeUser(function(user,cb){
    cb(null, user.id);
});

passport.deserializeUser((id,cb)=>{
    Usuario.findById(id,(err,usuario)=>{
        cb(err,usuario);
    });
});



module.exports = passport;