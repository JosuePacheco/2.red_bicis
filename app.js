require('dotenv').config();
require('newrelic');

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const passport = require('./config/passport');
const session = require('express-session');
var MongoDBStore = require('connect-mongodb-session')(session);
const jwt =require('jsonwebtoken');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/usuario');
var tokenRouter = require('./routes/token');
var bicicletasRouter = require('./routes/bicicletas');
var bicicletasAPIRouter = require('./routes/api/bicicletas');
var usuariosAPIRouter = require('./routes/api/usuarios');

let store;
if(process.env.NODE_ENV === 'development'){
  store= new session.MemoryStore;
}else{
  store = new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection: 'sessions'
  });
  store.on('error',(error)=>{
    assert.ifError(error);
    assert.ok(false);
  });
}

var app = express();
app.use(session({
  cookie: {maxAge: 240*60*60*1000},
  store: store,
  saveUninitialized : true,
  resave: true,
  secret: 'red_bicis_!!!**!!'
}));
//mongose
var mongoose = require('mongoose');
const usuario = require('./models/usuario');
const { token } = require('morgan');
const { assert } = require('console');
var mongoDB = process.env.MONGO_URI;  
mongoose.connect(mongoDB,{useNewUrlParser:true, useUnifiedTopology: true});
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error',console.error.bind(console,'MongoDB connection error'));


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/login',(req,res)=>{
  res.render('session/login')
});

app.post('/login',(req,res,next)=>{
  //passport
  passport.authenticate('local',(err,usuario,infor)=>{
    if(err) return next(err);
    if(!usuario) return res.render('session/login',{infor});
    req.login(usuario,(err)=>{
      if(err) return next(err);
      return res.redirect('/');
    });
  })(req,res,next);
});

app.get('/logout',(req,res)=>{
  req.logout();
  res.redirect('/');
});

app.get('/forgotPassword',(req,res)=>{
  res.render('session/forgotPassword');
});

app.post('/forgotPassword',(req,res)=>{
  usuario.findOne({email:req.body.email},(err,usuario)=>{
    if(!usuario) return res.render('session/forgotPassword',{info:{message:'No existe el email para un usuario existente'}});
    usuario.resetPassword((err)=>{
      if(err) return next(err);
      console.log('session/forgotPasswordMessage');
    });
      res.render('session/forgotPasswordMessage');
  });
});

app.get('/resetPassword/:token',(req,res,next)=>{
  token.findOne({token:req.params.token},(err,token)=>{
    if(!token) return res.status(400).send({type:'not-verified',msg:'No existe un usuario asociado al token. Verifique que su token no haya exporado'});
    usuario.findById(token._userId,(err,usuario)=>{
      if(!usuario) return res.status(400).send({msg:'No existe un usuario asociado al token'});
      res.render('session/resetPassword',{errors:{},usuario:usuario})
    })
  })
});

app.post('/resetPassword',(req,res)=>{
  if(req.body.password != req.body.confirm_password){
    res.render('session/resetPassword',{errors:{confirm_password:{message:'no coincide el password ingresado'}},
  usuario: new usuario({email:req.body.email})});
  return;
  }
  usuario.findOne({email:req.body.email},(err,usuario)=>{
    usuario.password=req.body.password;
    usuario.save((err)=>{
      if(err){
        res.render('session/resetPassword',{errors:err.errors,usuario: new Usuario({email: req.body.email})});
      }else{
        res.redirect('/login');
      }
    });
  });
});


app.use('/', indexRouter);
app.use('/usuarios', usersRouter);
app.use('/bicicletas',bicicletasRouter);
app.use('/token',tokenRouter);
app.use('/api/bicicletas',bicicletasAPIRouter);
app.use('/api/usuarios',usuariosAPIRouter);

app.use('/privacy_policy',(req,res)=>{
 res.sendFile('public/privacy_policy.html');
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loggedIn(req,res,next){
  if(req.user){
    next();
  }else{
    console.log('user sin login');
    res.redirect('/login');
  }
}

app.get('/auth/google',
  passport.authenticate('google', { scope: 
      [ 'https://www.googleapis.com/auth/plus.login',
      , 'https://www.googleapis.com/auth/plus.profile.emails.read' ] }
));

app.get( '/auth/google/callback', 
    passport.authenticate( 'google', { 
        successRedirect: '/',
        failureRedirect: '/error'
}));

module.exports = app;
