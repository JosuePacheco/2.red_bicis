var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

beforeEach(()=>{
    Bicicleta.allBicis=[];
});
describe('Bicicleta API',()=>{
    describe('Get Bicicletas /',()=>{
        it('Status 200',()=>{
            expect(Bicicleta.allBicis.length).toBe(0);
            
            var a = new Bicicleta(1,'rojo','urbana',[-34.6012424,-58.3861497]);
            Bicicleta.add(a);
            
            request.get('http://localhost:3000/api/bicicletas',(error,response,body)=>{
                expect(response.statusCode).toBe(200);
            });
        });
    })

    describe('post bicicletas /create',()=>{
        it('status 200',(done)=>{
            var headers = {'content-type':'application/json'};
            var aBici = '{"id":10,"color":"rojo","modelo":"urbana","lat":-34,"lng":-54}';
            request.post({
                headers:headers,
                url:'http://localhost:3000/api/bicicletas/create',
                body:aBici
            },(error,response,body)=>{
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe('rojo');
                done();
            });
        });
    });
});