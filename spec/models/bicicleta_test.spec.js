var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');

describe('Testing Bicicletas',()=>{
    //conectar la base de prueba
    beforeEach((done)=>{
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB,{useNewUrlParser:true, useUnifiedTopology: true});
        const db = mongoose.connection;
        db.on('error',console.error.bind(console,'connection error'));
        db.once('open',()=>{
            console.log('We are connected to test database!');
            done();
        })
    });
    //borrar los datos de prueba
    afterEach((done)=>{
        Bicicleta.deleteMany({},(err,success)=>{
            if(err) console.log(err);
            done();
        });
    });

    //crear una instancia
    describe('Bicicleta.createInstance',()=>{
        it('crea una instancia de bicicletas',()=>{
            var bici = Bicicleta.createInstance(1,"verde","urbana",[-34.5,-54.1]);
            expect(bici.code).toBe(1);
            expect(bici.color).toBe('verde');
            expect(bici.modelo).toBe('urbana');
            expect(bici.ubicacion[0]).toBe(-34.5);
            expect(bici.ubicacion[1]).toBe(-54.1);
        });
    });

    describe('Bicicleta.allBicis',()=>{
        it('comienza vacia',(done)=>{
            Bicicleta.allBicis((err,bicis)=>{
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add',()=>{
        it('agregamos solo una bici',(done)=>{
            var aBici = new Bicicleta({code:1,color:"verde",modelo:"urbana"});
            Bicicleta.add(aBici,(err,newBici)=>{
                if(err) console.log(err);
                Bicicleta.allBicis((err,bicis)=>{
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    done();
                });
            });
        });
    });


    describe('Bicicleta.findByCode',()=>{
        it('debe devolver la bici con code 1',(done)=>{
            Bicicleta.allBicis((err,bicis)=>{
                expect(bicis.length).toBe(0);

                var bici = new Bicicleta({code:1,color:"verde",modelo:"urbana"});
                Bicicleta.add(bici,(err,newBici)=>{
                    if(err) console.log(err);

                    var bici2 = new Bicicleta({code:2,color:"roja",modelo:"urbana"});
                    Bicicleta.add(bici2,(err,newBici)=>{
                        if(err) console.log(err);
                        Bicicleta.findByCode(1,(err,targetBici)=>{
                            expect(targetBici.code).toBe(bici.code);
                            expect(targetBici.color).toBe(bici.color);
                            expect(targetBici.modelo).toBe(bici.modelo);

                            done();

                        })
                    });

                })
            });
        })
    });

});




/* 
//TEST SIN MOOGSE
beforeEach(()=>{
    Bicicleta.allBicis=[];
});
describe('Bicicleta.allBicis',()=>{
    it('comienza vacia',()=>{
        expect(Bicicleta.allBicis.length).toBe(0);
    });
}); 

describe('Bicicleta.add',()=>{
    it('agregamos una',()=>{
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1,'rojo','urbana',[-34.6012424,-58.3861497]);
        Bicicleta.add(a);
        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe('Bicicleta.findById',()=>{
    it('debe devolver la bici con id 1',()=>{
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1,'rojo','urbana',[-34.6012424,-58.3861497]);
        var b = new Bicicleta(2,'rojo','urbana',[-34.6012424,-58.3861497]);
        Bicicleta.add(a);
        Bicicleta.add(b);
        var targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1);
    });
});
*/